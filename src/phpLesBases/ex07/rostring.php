<?php

// Récupérer uniquement le PREMIER paramètre
// Gérer le cas où celui-ci est vide
// Mettre le premier mot de ce paramètre en DERNIERE place
// Réafficher le tout séparer d'un espace

if (!isset($argv[1])) {
    exit();
}
$abc = $argv[1];
// Récupérer le paramètre 1 du terminal
$tableau = explode(' ', $abc);
// Transformer le string en array (tableau)

array_push($tableau, $tableau[0]);
array_shift($tableau);
// Effectuer l'organisation du tableau comme demandé

$bca = implode(' ', $tableau);
$stringfinal = preg_replace('/\s+/', ' ', $bca);
// Remettre le tableau sous forme de string et supprimer
// les espaces invisibles

echo $stringfinal . "\n";
// Afficher le résultat
